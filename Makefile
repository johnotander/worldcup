.PHONY: build clean fmt install test

build:
	go build worldcup.go models.go query_params.go

clean:
	rm main

install:
	./bin/install.sh

test:
	go test . -cover

benchmark:
	go test -bench=. -benchtime=10s

fmt:
	go fmt .

report:
	echo "Coverage\n" > report.txt
	make test >> report.txt
	echo "\n--------\n\nBenchmarks\n" >> report.txt
	make benchmark >> report.txt
