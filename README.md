# worldcup

Golang client for the [Women's World Cup 2015][wwc] API based on [resty][resty].

My initial idea was to write a client for the World Cup 2018, but they unfortunately don't seem to have a public API (and it hasn't started yet).
I was able to find a public community API for the previous Women's World Cup that still returns all the results so I built a client for their API.

## Caveats

I'm still wrapping my head around Go so there are likely multiple places where I'm not abiding by proper conventions.
I'm currently reading _The Go Programming Language_, in order to learn more.

## What I learned

- Structs and adding methods to them
- Golang typing
- Testing
- JSON marshaling
- Using the resty library (and libraries in general)
- Mocking HTTP requests

## Places to improve

Rather than stubbing at the HTTP request layer, it'd likely be preferable to use something like [vcr][vcr] to record live HTTP requests and then replay them for future test runs.
This'd be preferable so that the responses can be refreshed periodically for a more thorough integration test, while still keeping the test suite fast.

I also was unable to use the `SetError` and `Error` methods in resty so I added in custom error checking/handling.
I don't think this is being handled correctly so it's definitely something I'll need to read up on.
I believe my error handling in general deviates from community convention.

Not all endpoints were implemented, but they aren't usable at this point anyway because they're for live results.

It'd also be nice to implement all the endpoints.
I didn't have enough time to finish implementing them all.

## Installation

```sh
git clone https://bitbucket.org/johnotander/worldcup
cd worldcup
make install
```

## Usage

See the [demo](./example/README.md) for a trivial app implementation.

```go
client := worldcup.New()
```

#### `GetMatches`

`GetMatches` retrieves all World Cup matches.
It returns on array of `Match` structs.
The `Match` definition:

```go
type Match struct {
	Number         int        `json:"match_number"`
	Location       string     `json:"location"`
	Status         string     `json:"status"`
	WinnerCode     string     `json:"winner_code"`
	HomeTeam       TeamResult `json:"home_team"`
	AwayTeam       TeamResult `json:"away_team"`
	HomeTeamEvents []Event    `json:"home_team_events"`
	AwayTeamEvents []Event    `json:"away_team_events"`
}
```

##### Options

```go
client := worldcup.New()
query := worldcup.QueryParams{Order: "total_goals"}

result, err := client.GetMatches(query)
```

`GetMatches` accepts the following options.

###### Order `string`(optional)

Value | Description
--- | ---
`asc` | Order matches in ascending order
`desc` | Order matches in descending order
`total_goals` | Order by the total goals of match
`closest_score` | Order matches by lowest goal differential
`away_team_goals` | Order matches by amount of away team goals
`home_team_goals` | Order matches by amount of home team goals

#### `GetMatchesByCountry`

`GetMatchesByCountry` returns the same data as `GetMatches` but filtered based on country.

```go
client := worldcup.New()

result, err := client.GetMatchesByCountry("USA")
```

#### `GetTeams`

`GetTeams` retrieves all World Cup teams.
It returns on array of `Team` structs.
The `Team` definition:

```go
type Team struct {
	Country string `json:"country"`
	Code    string `json:"fifa_code"`
	GroupId int    `json:"group_id"`
}
```

## Development

The primary entrypoint for dev tasks is in the Makefile.

###### Build the project

This project is built as a library, so you need to `cd` into the `example` dir to run an app build which outputs a binary that can be run.

```sh
cd example
make install
make build
```

###### Run test suite

Network requests are mocked and return a static JSON response (one for a successful attempt and one for an API error).

```sh
make test
```

###### Run library report

The report aggregates the test coverage and benchmarking and writes to [`report.txt`](./report.txt).

```sh
make report
```

[wwc]: http://worldcup.sfg.io/
[wwc-gh]: https://github.com/estiens/world_cup_json
[resty]: https://github.com/go-resty/resty
[vcr]: https://github.com/dnaeon/go-vcr
