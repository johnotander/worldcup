#!/usr/bin/env sh

set -e

go get -u github.com/go-resty/resty
go get -u github.com/olekukonko/tablewriter
