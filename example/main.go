package main

import (
	"os"
	"strconv"

	"bitbucket.org/johnotander/worldcup"
	"github.com/olekukonko/tablewriter"
)

func main() {
	client := worldcup.New()
	matches, err := client.GetMatchesByCountry("USA")

	if err != nil {
		panic(err)
	}

	var tableData [][]string

	for _, match := range matches {
		home := match.HomeTeam
		away := match.AwayTeam

		number := strconv.Itoa(match.Number)
		homeScore := strconv.Itoa(home.Goals)
		awayScore := strconv.Itoa(away.Goals)

		score := home.Code + " " + homeScore + " - " + away.Code + " " + awayScore
		tableData = append(tableData, []string{number, match.WinnerCode, score, match.Location})
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Number", "WINNER", "SCORE", "LOCATION"})
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.AppendBulk(tableData)
	table.Render()
}
