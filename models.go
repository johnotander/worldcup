package worldcup

const DRAW = "Draw"

type Team struct {
	Country string `json:"country"`
	Code    string `json:"fifa_code"`
	GroupId int    `json:"group_id"`
}

type TeamResult struct {
	Code  string `json:"code"`
	Goals int    `json:"goals"`
}

type Event struct {
	Id     int    `json:"id"`
	Minute string `json:"time"`
	Type   string `json:"type_of_event"`
	Player string `json:"player"`
}

type Match struct {
	Number         int        `json:"match_number"`
	Location       string     `json:"location"`
	Status         string     `json:"status"`
	WinnerCode     string     `json:"winner_code"`
	HomeTeam       TeamResult `json:"home_team"`
	AwayTeam       TeamResult `json:"away_team"`
	HomeTeamEvents []Event    `json:"home_team_events"`
	AwayTeamEvents []Event    `json:"away_team_events"`
}

func (m *Match) Winner() *TeamResult {
	if m.IsDraw() {
		return nil
	} else if m.HomeTeam.Code == m.WinnerCode {
		return &m.HomeTeam
	} else if m.AwayTeam.Code == m.WinnerCode {
		return &m.AwayTeam
	} else {
		return nil
	}
}

func (m *Match) IsDraw() bool {
	return m.WinnerCode == DRAW
}
