package worldcup

import (
	"testing"
)

func TestMatchWinner(t *testing.T) {
	winner := TeamResult{Code: "USA", Goals: 2}
	loser := TeamResult{Code: "GER", Goals: 1}

	match := Match{
		WinnerCode: winner.Code,
		HomeTeam:   winner,
		AwayTeam:   loser,
	}

	result := match.Winner()

	if result.Code != winner.Code {
		t.Errorf("expected %s as winner, got %s", winner.Code, result.Code)
	}
}

func TestMatchWinnerDraw(t *testing.T) {
	match := Match{WinnerCode: DRAW}

	if result := match.Winner(); result != nil {
		t.Errorf("expected nil as winner, got %s", result.Code)
	}
}

func TestMatchDraw(t *testing.T) {
	match := Match{WinnerCode: DRAW}

	if result := match.IsDraw(); !result {
		t.Errorf("expected match to be a draw")
	}
}

func BenchmarkWinner(b *testing.B) {
	winner := TeamResult{Code: "USA", Goals: 2}
	loser := TeamResult{Code: "GER", Goals: 1}

	match := Match{
		WinnerCode: winner.Code,
		HomeTeam:   winner,
		AwayTeam:   loser,
	}

	for n := 0; n < b.N; n++ {
		match.Winner()
	}
}

func BenchmarkWinnerDraw(b *testing.B) {
	match := Match{WinnerCode: DRAW}

	for n := 0; n < b.N; n++ {
		match.Winner()
	}
}

func BenchmarkIsDraw(b *testing.B) {
	match := Match{WinnerCode: DRAW}

	for n := 0; n < b.N; n++ {
		match.IsDraw()
	}
}
