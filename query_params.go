package worldcup

type QueryParams struct {
	Order   string
	Country string
}

func (q *QueryParams) ToMap() map[string]string {
	query := make(map[string]string)

	if q.Order == "asc" || q.Order == "desc" {
		query["by_date"] = q.Order
	} else if q.Order != "" {
		query["by"] = q.Order
	}

	if q.Country != "" {
		query["fifa_code"] = q.Country
	}

	return query
}

func (q *QueryParams) HasCountry() bool {
	return q.Country != ""
}
