package worldcup

import "testing"

func TestToMap(t *testing.T) {
	query := QueryParams{Country: "USA", Order: "total_goals"}

	result := query.ToMap()

	if result["by"] != query.Order {
		t.Errorf("map.by expected %s, got %s", query.Order, result["by"])
	}

	if result["fifa_code"] != query.Country {
		t.Errorf("map.fifa_code expected %s, got %s", query.Country, result["fifa_code"])
	}

	query.Order = "asc"
	result = query.ToMap()

	if result["by"] != "" {
		t.Errorf("map.by expected empty, got %s", result["by"])
	}

	if result["by_date"] != query.Order {
		t.Errorf("map.by expected %s, got %s", query.Order, result["by"])
	}
}

func TestHasCountry(t *testing.T) {
	query := QueryParams{Country: "USA"}

	if !query.HasCountry() {
		t.Errorf("expected true, got false")
	}

	query.Country = ""
	if query.HasCountry() {
		t.Errorf("expected false, got true")
	}
}

func BenchmarkToMap(b *testing.B) {
	query := QueryParams{Country: "USA", Order: "total_goals"}

	for n := 0; n < b.N; n++ {
		query.ToMap()
	}
}

func BenchmarkHasCountry(b *testing.B) {
	query := QueryParams{Country: "USA"}

	for n := 0; n < b.N; n++ {
		query.HasCountry()
	}
}
