package worldcup

import (
	"errors"
	"fmt"

	"github.com/go-resty/resty"
)

const API_BASE = "http://worldcup.sfg.io"

type Client struct {
	RestClient *resty.Client
}

func New() *Client {
	client := resty.New()
	client.SetHostURL(API_BASE)

	return &Client{RestClient: client}
}

func (c *Client) GetTeams() ([]Team, error) {
	var teams []Team

	resp, err := c.RestClient.R().
		SetResult(&teams).
		Get("/teams")

	if hasError(err, resp) {
		return nil, handleError(err, "teams", resp.StatusCode())
	}

	return teams, nil
}

func (c *Client) GetMatches(query QueryParams) ([]Match, error) {
	var matches []Match
	var path string

	if query.HasCountry() {
		path = "/matches/country"
	} else {
		path = "/matches"
	}

	resp, err := c.RestClient.R().
		SetQueryParams(query.ToMap()).
		SetResult(&matches).
		Get(path)

	if hasError(err, resp) {
		return nil, handleError(err, "matches", resp.StatusCode())
	}

	return matches, nil
}

func (c *Client) GetMatchesByCountry(country string) ([]Match, error) {
	var query = QueryParams{Country: country}
	return c.GetMatches(query)
}

func handleError(err error, endpoint string, status int) error {
	if err != nil {
		return err
	} else {
		error_string := fmt.Sprintf("Received %d from %s endpoint", status, endpoint)
		return errors.New(error_string)
	}
}

func hasError(err error, resp *resty.Response) bool {
	if err != nil || resp.StatusCode() > 399 {
		return true
	} else {
		return false
	}
}
