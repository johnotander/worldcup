package worldcup

import (
	"testing"
)

const TOTAL_TEAMS = 24
const TOTAL_USA_MATCHES = 7
const MOST_GOALS_IN_MATCH = 11

func TestGetTeams(t *testing.T) {
	client := New()

	result, err := client.GetTeams()

	if err != nil {
		t.Errorf("error requesting teams")
	}

	count := len(result)

	if count != TOTAL_TEAMS {
		t.Errorf("returned incorrect number of teams, wanted %d, got %d", TOTAL_TEAMS, count)
	}
}

func TestGetMatches(t *testing.T) {
	client := New()
	query := QueryParams{Order: "total_goals"}

	result, err := client.GetMatches(query)

	if err != nil {
		t.Errorf("error requesting team, wanted list, got error")
	}

	match := result[0]
	totalGoals := match.HomeTeam.Goals + match.AwayTeam.Goals

	if totalGoals != MOST_GOALS_IN_MATCH {
		t.Errorf("didn't sort by total goals, wanted %d, got %d", MOST_GOALS_IN_MATCH, totalGoals)
	}
}

func TestGetMatchesError(t *testing.T) {
	client := New()
	query := QueryParams{Country: "FOO"}

	_, err := client.GetMatches(query)

	if err == nil {
		t.Errorf("expected an error, go nil")
	}
}

func TestGetMatchesByCountry(t *testing.T) {
	client := New()

	result, err := client.GetMatchesByCountry("USA")

	if err != nil {
		t.Errorf("error requesting team, wanted USA, got error")
	}

	count := len(result)

	if count != TOTAL_USA_MATCHES {
		t.Errorf("returned incorrect number of matches, wanted %d, got %d", TOTAL_USA_MATCHES, count)
	}
}

// This is commented out because the primary overhead of these client calls is
// network IO and requests to the API. By running it we'd likely DoS them without
// network stubbing.
//
// func BenchmarkGetMatchesByCountry(b *testing.B) {
//   for n := 0; n < b.N; n++ {
// 		client := New()
//
// 		if _, err := client.GetMatchesByCountry("USA"); err != nil {
// 			panic(err)
// 		}
//   }
// }
